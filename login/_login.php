<?php
/**
 * Customized login page
 **/

// Enqueue Login CSS
function my_logincustomCSSfile() {
  wp_enqueue_style('login-styles', get_stylesheet_directory_uri() . '/login/login_styles.css');
}
add_action('login_enqueue_scripts', 'my_logincustomCSSfile');

// Change logo image URL
function awspain_loginURL()
{
  return get_bloginfo('url');
}
add_filter('login_headerurl', 'awspain_loginURL');

// Change login URL title attr
function awspain_loginURLtext()
{
  return get_bloginfo('name');
}
add_filter('login_headertitle', 'awspain_loginURLtext');

function awspain_loginfooter() { ?>
  <p id="login-footer">
    <a href="http://www.planxdesign.eu/">planXdesign &copy; 2014</a>
  </p>
<?php }
add_action('login_footer','awspain_loginfooter');

